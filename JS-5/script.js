let obj = {
            id: 323,
            name: 'Some',
            insideObj: {
                name: 'Inside',
                oneMoreObj: {
                    name: 'oneMore'
                }
            },
            end: true
        };

        function cloneObjFunc(obj) {
            let cloneObj = {}
            for(let elem in obj){
                if(typeof(obj[elem]) == 'object'){
                    cloneObj[elem] = cloneObjFunc(obj[elem])
                }else{
                    cloneObj[elem] = obj[elem]
                }
            }
            return cloneObj;
        }
        console.log(cloneObjFunc(obj))
