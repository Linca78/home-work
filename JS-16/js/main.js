$(document).ready(function() {
    let click = 0;
    let block = localStorage.getItem('block');
    $('.changecolor').click(function(event) {
        if (click == 0) {
            $('.wrapper').addClass('body-color');
            click = 1;
            localStorage.setItem('block', 'true');

        } else {
            $('.wrapper').removeClass('body-color');
            click = 0;
            localStorage.setItem('block', 'false');
        }
    });

    if (block == 'true') {
        $('.wrapper').addClass('body-color');
        click = 1;
    }
    if (block == 'false') {
        click = 0;
        $('.wrapper').removeClass('body-color');
    }
});