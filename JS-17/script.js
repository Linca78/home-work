$('#btn').on('click', () => {
    let inpH = document.createElement('input');
    $(inpH).attr({ type: 'number', name: 'input', placeholder: ' введите число' }).css({ 'marginTop': '10px', 'display': 'block' });

    let inpColor = document.createElement('input');
    $(inpColor).attr({ type: 'text', name: 'input2', placeholder: ' введите цвет' }).css({ 'marginTop': '10px', 'display': 'block' });

    let btnPaint = document.createElement('button');
    $(btnPaint).attr('type', 'submit').text('Нарисовать').css('marginTop', '10px');
    $('body').append(inpH);
    $('body').append(inpColor);
    $('body').append(btnPaint);

    $(btnPaint).on('click', () => {
        let circle = document.createElement('div');
        $(circle).css({ 'marginTop': '20px', "width": `${inpH.value}px`, 'height': `${inpH.value}px`, 'borderRadius': '50%', 'backgroundColor': `${inpColor.value}`});

        if (inpH.value.length == 0 || inpColor.value.length == 0) {
            alert("Вы не ввели значение ");
        } else
            $('body').append(circle);
    });
});