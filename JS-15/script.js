let time = 0;
let running = 0;

function startPause() {
    if (running == 0) {
        running = 1;
        increment();
        let start = document.getElementById('start').innerHTML = 'Pause';
        let startPause = document.getElementById('startPause');
    } else {
        running = 0;
        start.innerHTML = 'Pause';
        startPause.style = 'backgroundColor: #7CFC00; borderColor: #7CFC00; outline: none';
    }
}

function reset() {
    running = 0;
    start.innerHTML = 'Start';
    let output = document.getElementById('output').innerHTML = '00:00:00';
    startPause.style = 'backgroundColor: #7CFC00; borderColor: #7CFC00; outline: none';
}

function increment() {
    if (running == 1) {
        setTimeout(function() {
            time++;
            let min = Math.floor(time / 10 / 60);
            let sec = Math.floor(time / 10 % 60);
            let tenth = time % 10;
            if (min < 10) {
                min = '0' + min;
            }
            if (sec < 10) {
                sec = '0' + sec;
            }
            output.innerHTML = `${min}:${sec}:${tenth}0`;
            increment();
        }, 100)
    }
};