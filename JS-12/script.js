let btn = document.getElementById('btn')
btn.addEventListener('click', () => {
    let inpH = document.createElement('input')
    inpH.type = 'number';
    inpH.name = 'input';
    inpH.style.display = 'block';
    inpH.style.marginTop = '10px';
    inpH.placeholder = 'введите число'
    let inpColor = document.createElement('input')
    inpColor.type = 'text';
    inpColor.name = 'input2';
    inpColor.style.display = 'block';
    inpColor.style.marginTop = '10px';
    inpColor.placeholder = 'введите цвет'
    let btnPaint = document.createElement('button')
    btnPaint.textContent = 'Нарисовать';
    btnPaint.type = 'submit';
    btnPaint.style.marginTop = '10px';
    document.body.appendChild(inpH);
    document.body.appendChild(inpColor);
    document.body.appendChild(btnPaint);
    
    btnPaint.addEventListener('click', () => {
        let circle = document.createElement('div')
        circle.style.marginTop = '20px';
        circle.style.width = `${inpH.value}px`;
        circle.style.height = `${inpH.value}px`;
        circle.style.borderRadius = '50%';
        circle.style.backgroundColor = `${inpColor.value}`;
        if (inpH.value.length == 0 || inpColor.value.length == 0) {
            alert("Вы не ввели значение ");
        } else
            document.body.appendChild(circle);
    });
});